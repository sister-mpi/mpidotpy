# import mpi4py
import numpy, time
from mpi4py import MPI

# import library random untuk generate angka integer secara random
from random import random

com = MPI.COMM_WORLD
rank = com.Get_rank()

# dapatkan total proses berjalan
size = com.Get_size()

# generate angka integer secara random untuk setiap proses
generate = numpy.random.randint(10, size=2)


# lakukam penjumlahan dengan teknik reduce, root reduce adalah proses dengan rank 0
print("Dari rank ", rank, " menyumbang angka", generate[0])

hasil = com.reduce(generate[0], op =MPI.SUM , root = 0)

# jika saya proses dengan rank 0 maka saya akan menampilkan hasilnya
if rank == 0:
    time.sleep(0.1) #agar selalu tampil paling akhir, dikasih jeda 0.1
    print("saya rank ", rank, " menghitung, dan jumlahnya adalah ", hasil)
