# import mpi4py
from mpi4py import MPI
import time
# buat fungsi dekomposisi bernama local_loop
# local_loop akan menghitung setiap bagiannya
# gunakan 4/(1+x^2), perhatikan batas awal dan akhir untuk dekomposisi
# misalkan size = 4 maka proses 0 menghitung 0-25, proses 1 menghitung 26-50, dst
def local_loop(num_steps,begin,end):
    step = 1/num_steps
    sum = 0
    # 4/(1+x^2)
    for i in range(begin,end):
        x =  (i+0.5)*step
        sum = sum + 4/(1+x**2)
    # print (sum)
    return sum

# fungsi Pi
def Pi(num_steps):
    start = time.time()

    # buat COMM
    com = MPI.COMM_WORLD

    # dapatkan rank proses
    rank = com.Get_rank()

    # dapatkan total proses berjalan
    size = com.Get_size()

    # buat variabel baru yang merupakan num_steps/total proses
    step = int(num_steps/size)

    # cari local_sum
    # local_sum merupakan hasil dari memanggil fungsi local_loop

    #Mencari nilai begin dan end
    begin = (step * rank)
    end = begin + step
    # print(begin , " ", end)


    local_sum = local_loop(num_steps,begin,end)

    # lakukan penjumlahan dari local_sum proses-proses yang ada ke proses 0
    # bisa digunakan reduce atau p2p sum
    hasil = com.reduce(local_sum, op =MPI.SUM , root = 0)

    # jika saya proses dengan rank 0  maka tampilkan hasilnya
    if rank == 0:
        pi = hasil / num_steps
        end = time.time()
        print ("Pi with %d steps is %f in %f secs" %(num_steps, pi, end-start))

# panggil fungsi utama
if __name__ == '__main__':
    Pi(100000)
