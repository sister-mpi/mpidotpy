# import mpi4py
import numpy
from mpi4py import MPI

# import library random untuk generate angka integer secara random
from random import random

com = MPI.COMM_WORLD

rank = com.Get_rank()

# dapatkan total proses berjalan
size = com.Get_size()

# generate angka integer secara random untuk setiap proses
generate = numpy.random.randint(10, size=1)

# jika saya adalah proses dengan rank 0 maka:
# saya menerima nilai dari proses 1 s.d proses dengan rank terbesar
# menjumlah semua nilai yang didapat (termasuk nilai proses saya)
if rank == 0  :
	print("Dari rank ", rank, " menyumbang angka", generate[0])
	sum = generate[0]
	for i in range(1, size):
		sum = sum + com.recv(source = i)[0]
	print("saya rank ", rank, " menghitung, dan jumlahnya adalah ", sum)

# jika bukan proses dengan rank 0, saya akan mengirimkan nilai proses saya ke proses dengan rank=0
else:
	print("Dari rank ", rank, " menyumbang angka", generate[0])
	com.send(generate, dest = 0)
