# import mpi4py
import numpy
from mpi4py import MPI
import random
# buat COMM
com = MPI.COMM_WORLD
# dapatkan rank proses
rank = com.Get_rank()
# dapatkan total proses berjalan
size = com.Get_size()

randNum = numpy.random.randint(10, size=2)
# jika saya rank terbesar maka saya akan mengirimkan pesan ke proses yang mempunyai rank 0 s.d rank terbesar-1
if rank == size - 1:
	print("\n dari process",rank, "sender mengirimkan angka random bernilai", randNum[0],'\n')
	com.Send(randNum, dest= rank - 1)

# jika saya bukan rank terbesar maka saya akan menerima pesan yang berasal dari proses dengan rank terbesar
else:
	print("Dari process", rank, "sebelum menerima dari sender, reicever mengatakan", randNum[0])
	com.Recv(randNum, source= rank + 1)
	print("Dari process", rank, "menerima pesan dari sender, sender mengatakan", randNum[0],'\n')
	com.Send(randNum, dest= rank - 1)
