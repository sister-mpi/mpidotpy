## import mpi4py
import numpy
from mpi4py import MPI
import random
# buat COMM
com = MPI.COMM_WORLD

# dapatkan rank proses
rank = com.Get_rank()

# dapatkan total proses berjalan
size = com.Get_size()


# jika saya rank 0 maka saya akan melakukan broadscast
if rank == 0  :
	broadscast = ("Hai ini BC dari master node, yaitu node ", rank)



# jika saya bukan rank 0 maka saya menerima pesan
else:
	broadscast = None

data = com.bcast(broadscast, root = 0)
print("rank saya ", rank , data)
